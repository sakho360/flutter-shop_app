import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class productList extends StatefulWidget {
  @override
  _productListState createState() => _productListState();
}

class _productListState extends State<productList> {
  var product_list=[
    {
    "name":"bloze",
    "picture" :'images/products/blazer1.jpeg',
    "oldprice":100,
    "price":60,
  },
    {
      "name":"dress",
      "picture" :'images/products/dress1.jpeg',
      "oldprice":120,
      "price":40,
    },

];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_list.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context,int index){
       return single_product(
           product_name:product_list[index]['name'],
            product_picture:product_list[index]['picture'],
             product_oldprice:product_list[index]['oldprice'],
             product_price:product_list[index]['price'],
       );
      },
    );
  }
}
class single_product extends StatelessWidget {
  final product_name;
  final product_picture;
  final product_oldprice;
  final product_price;
  single_product({
    this.product_name,
    this.product_picture,
    this.product_oldprice,
    this.product_price,
});
  @override
  Widget build(BuildContext context) {
    return
      Card(
      child: Hero(
        tag: product_name,
        child: Material(
          child: InkWell(
            onTap: (){},
            child: GridTile(
              footer: Container(
                color: Colors.white,
                child: ListTile(
                  leading: Text(product_name,
                  style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              child:
//              Text('salam'),
              Image.asset(product_picture,
                fit: BoxFit.cover,

              ),
            ),
          ),
        ),
      ),
    );

  }
}

